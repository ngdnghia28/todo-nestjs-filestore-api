import { initializeApp, firestore, credential, ServiceAccount } from 'firebase-admin';
import { ConfigService } from '../config/config.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class FirestoreService {
    private firestore: firestore.Firestore;
    constructor(private readonly configService: ConfigService) {
        const firestorePath: string = this.configService.get('FILESTORE_KEY_PATH');
        const serviceAccount = require(firestorePath);

        initializeApp({
            credential: credential.cert(serviceAccount as ServiceAccount),
        });

        this.firestore = firestore();
    }

    getConnection() {
        return this.firestore;
    }
}
