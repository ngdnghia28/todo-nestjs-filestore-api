import { Controller, Post, Body, Get, Param, Put, Delete, HttpCode, ValidationPipe } from '@nestjs/common';
import { CreateTodoDto } from './dto/create-todo.dto';
import { TodosService } from './todos.service';
import { Todo } from './interfaces/todo.interface';

@Controller('todos')
export class TodosController {
    constructor(private readonly todosService: TodosService) { }

    @Post()
    create(@Body(new ValidationPipe()) createTodo: CreateTodoDto) {
        return this.todosService.create(createTodo as Todo);
    }

    @Get()
    findAll() {
        return this.todosService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.todosService.get(id);
    }

    @Put(':id')
    update(@Param('id') id: string, @Body(new ValidationPipe()) updateTodoDto: CreateTodoDto) {
        return this.todosService.update(id, updateTodoDto as Todo);
    }

    @Delete(':id')
    @HttpCode(204)
    remove(@Param('id') id: string) {
        return this.todosService.delete(id);
    }
}
