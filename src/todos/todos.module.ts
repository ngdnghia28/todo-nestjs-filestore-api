import { Module } from '@nestjs/common';
import { TodosController } from './todos.controller';
import { TodosService } from './todos.service';
import { ServicesModule } from '../services/services.module';

@Module({
    imports: [ServicesModule],
    controllers: [TodosController],
    providers: [TodosService],
})
export class TodosModule { }
