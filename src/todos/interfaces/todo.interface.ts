export class Todo {
    id: string;
    user: string;
    name: string;
    startDate: Date;
    endDate: Date;
    effort: number;
    assignedUser: string;
}
