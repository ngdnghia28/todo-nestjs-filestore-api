import { IsString, IsInt, IsDateString } from 'class-validator';
export class CreateTodoDto {
    @IsString()
    readonly user: string;
    @IsString()
    readonly name: string;
    @IsDateString()
    readonly startDate: Date;
    @IsDateString()
    readonly endDate: Date;
    @IsInt()
    readonly effort: number;
    @IsString()
    readonly assignedUser: string;
}
