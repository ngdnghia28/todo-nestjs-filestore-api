import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { TodosModule } from '../todos.module';
import { TodosService } from '../todos.service';
import { INestApplication } from '@nestjs/common';
import { Todo } from '../interfaces/todo.interface';
import { deepEqual, equal } from 'assert';
import { FirestoreService } from '../../services/firestore.service';


describe('Todos', () => {
    let app: INestApplication;
    let todosService: TodosService;
    let fireStoreService: FirestoreService;
    async function deleteCollection(path: string) {
        // Get a new write batch
        const batch = fireStoreService.getConnection().batch();

        await fireStoreService.getConnection().collection(path).listDocuments().then(val => {
            val.map((vl) => {
                batch.delete(vl);
            });
        });
        await batch.commit();
    }

    beforeAll(async () => {
        const module = await Test.createTestingModule({
            imports: [TodosModule],
        })
            // .overrideProvider(TodosService)
            // .useValue(todosService)
            .compile();

        todosService = module.get<TodosService>(TodosService);
        fireStoreService = module.get<FirestoreService>(FirestoreService);
        app = module.createNestApplication();
        await app.init();
        await deleteCollection('todos');
    });

    it(`should be able to fetch todos list`, async () => {
        return request(app.getHttpServer())
            .get('/todos')
            .expect(200)
            .expect(
                await todosService.findAll());
    });

    it(`Should be able to get todo by Id`, async () => {
        const todo = {
            endDate: '2019-07-17T07:32:03.210Z',
            startDate: '2019-07-15T07:32:03.210Z',
            name: 'Doremon',
            user: '123',
            assignedUser: '123',
            effort: 8,
        };
        const docref = await fireStoreService.getConnection().collection('todos').add(todo);
        let newTodo: Todo;
        return request(app.getHttpServer())
            .get('/todos/' + docref.id)
            .expect(200)
            .expect(async (res) => {
                newTodo = res.body as Todo;
                delete newTodo.id;
                deepEqual(newTodo, todo);
            });
    });


    it(`Can create todo`, async () => {
        const todo = {
            endDate: '2019-07-17T07:32:03.210Z',
            startDate: '2019-07-15T07:32:03.210Z',
            name: 'Doremon',
            user: '123',
            assignedUser: '123',
            effort: 8,
        };
        let newTodo: Todo;
        return request(app.getHttpServer())
            .post('/todos')
            .send(todo)
            .expect(201)
            .expect(async (res) => {
                newTodo = res.body as Todo;
                const fsTodo = (await fireStoreService.getConnection().collection('todos').doc(newTodo.id).get()).data() as Todo;
                delete newTodo.id;
                deepEqual(newTodo, fsTodo);
            });
    });

    it(`Should be able to delete todo`, async () => {
        const todo = {
            endDate: '2019-07-17T07:32:03.210Z',
            startDate: '2019-07-15T07:32:03.210Z',
            name: 'Doremon',
            user: '123',
            assignedUser: '123',
            effort: 8,
        };
        const docref = await fireStoreService.getConnection().collection('todos').add(todo);
        let newTodo: Todo;
        return request(app.getHttpServer())
            .delete('/todos/' + docref.id)
            .expect(204)
            .expect(async (res) => {
                newTodo = res.body as Todo;
                const fsTodo = await fireStoreService.getConnection().collection('todos').doc(docref.id).get();
                equal(fsTodo.exists, false);
            });
    });

    it(`Should be able to update todo`, async () => {
        const todo = {
            endDate: '2019-07-17T07:32:03.210Z',
            startDate: '2019-07-15T07:32:03.210Z',
            name: 'Doremon',
            user: '12345678',
            assignedUser: '123',
            effort: 8,
        };
        const docref = await fireStoreService.getConnection().collection('todos').add(todo);
        return request(app.getHttpServer())
            .put('/todos/' + docref.id)
            .send({ ...todo, effort: 12 })
            .expect(200)
            .expect(async (res) => {
                const fsTodo = (await fireStoreService.getConnection().collection('todos').doc(docref.id).get()).data() as Todo;
                deepEqual({ ...todo, effort: 12 }, fsTodo);
            });
    });

    afterAll(async () => {
        await app.close();
        await deleteCollection('todos');
    });
});
