import { Injectable, NotFoundException } from '@nestjs/common';
import { Todo } from './interfaces/todo.interface';
import { FirestoreService } from '../services/firestore.service';
import { firestore } from 'firebase-admin';

@Injectable()
export class TodosService {
    private connection: firestore.Firestore;
    private todos: firestore.CollectionReference;

    constructor(private readonly firestoreService: FirestoreService) {
        this.connection = this.firestoreService.getConnection();
        this.todos = this.connection.collection('todos');
    }

    async get(id: string) {
        const doc = await this.todos.doc(id).get();
        if (!doc.exists) {
            throw new NotFoundException('No such document!');
        } else {
            const todo = doc.data() as Todo;
            todo.id = id;
            return todo;
        }
    }

    async create(todo: Todo) {
        const doc = await this.todos.add(todo);
        const newTodo: Todo = todo;
        newTodo.id = doc.id;
        return newTodo;
    }

    async update(id: string, todo: Todo) {
        const doc = await this.todos.doc(id).get();
        if (!doc.exists) {
            throw new NotFoundException('No such document!');
        } else {
            await this.todos.doc(id).update(todo);
            const newTodo: Todo = todo;
            newTodo.id = id;
            return newTodo;
        }
    }

    async delete(id: string) {
        const doc = await this.todos.doc(id).get();
        if (!doc.exists) {
            throw new NotFoundException('No such document!');
        } else {
            return await this.todos.doc(id).delete();
        }
    }

    async findAll() {
        const snapshot = await this.todos.get();
        if (snapshot.empty) {
            return [];
        }

        const todos: Todo[] = [];
        snapshot.forEach(doc => {
            const todo: Todo = doc.data() as Todo;
            todo.id = doc.id;
            todos.push(todo);
        });
        return todos;
    }
}
